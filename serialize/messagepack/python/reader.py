import csv,json
import sys
from io import BytesIO
import msgpack

def loadfile(filename):
    with open(filename, 'rb') as csvfile:
        datareader=csv.reader(csvfile,delimiter=':')
        i=0
        alignblob = {}
        iovinfo = {}
        headdatalist=[]
        corrdatalist=[]
        for row in datareader:
            i += 1
            #if (i > 44): 
            #    break 
            I=iter(row)
            print row,I
            nextk = I.next()
            key = ""
            if nextk is not None:
                key = nextk.strip()
            else:
                key = nextk
                
            val=[]
            if '#' in key:
                print 'Skip comment'
                continue
            if key == 'HFS':
                print 'Join line containing dates'
                val=':'.join(I)
                datadic = {key : val}
                headdatalist.append(datadic)
            elif 'H' in key:
                val = I.next().strip()
                datadic = {key : val}
                headdatalist.append(datadic)
            elif 'IOV' in key:
                val = I.next().strip()
                datadic = {key : val}
                iovinfo = val
            elif 'Corr' in key:
                print 'Correction line'
                val = I.next().split()
                print val
                typ = 'TYPE'
                typval = val[0]
                amdbid = 'AMDBID'
                amdbval = [int(c) for c in val[1:4]] 
                al = 'A'
                alval = [float(c) for c in val[4:10]] 
                #alval = val[5:11] 
                bl = 'B'
                blval = [float(c) for c in val[10:(len(val)-3)]] 
                #blval = val[11:(len(val)-3)]
                ch = 'CHAMBER'
                chval=val[len(val)-1]
                datadic = { typ : typval, amdbid : amdbval, al : alval, bl : blval, ch : chval}
                corrdatalist.append(datadic)
            else:
                val = (I.next()).strip()
                datadic = {key : val}
                print "ERROR: THIS DOES NOT BELONG TO OUTPUT ???"
                
            alignblob = { 'iovid' : iovinfo, 'header' : headdatalist, 'data' : corrdatalist }
            ##alignblob = { 'header' : headdatalist,'data' : corrdatalist }
    return alignblob

def packit(abdict):
    packed_dict = msgpack.packb(abdict)
    print packed_dict
    return packed_dict

def unpackit(packeddict):
    unpacked_dict = msgpack.unpackb(packeddict)
    print unpacked_dict
    
def unpackfield(obd):
    print 'Unpack single entries in ',obd
    buf = BytesIO()
    buf.write(obd)
    buf.seek(0)
    unpacker = msgpack.Unpacker(buf)
#    nel=unpacker.read_array_header()
#    for i in range(nel):
#        key = unpacker.unpack()
#        print i,' - Found key ',key
    
    nel=unpacker.read_map_header()
    print 'unpacking found map ',nel
    
def getfield(datalist, fieldname):
    dictionary = (item for item in datalist if fieldname in item.keys()).next()
    return dictionary[fieldname] 

def bindump(outf,obd):
    nfil=open(outf,'wb')
    ba=bytearray(obd)
    nfil.write(ba)
    nfil.close()

def binread(inpf):
    nfil=open(inpf,'rb')
    ba = nfil.read()
    nfil.close()
    return ba
    
def main(argv):
    inputfile = 'none'
    outputfile = 'out.mp'

    print 'Number of arguments:', len(argv), 'arguments.'
    print 'Argument List:', str(argv)
    if len(argv)>0:
        inputfile=argv[0]
        print 'Use inputfile ',inputfile
            
    dlist=[]
    dlist = loadfile(inputfile)
    print dlist
    pkd = packit(dlist)
    print '...and now unpack it'
    unpackit(pkd)
    bindump(outputfile,pkd)
    ##obd=binread(outputfile)
    ###unpackit(obd)
    ### this one does not work..... :
    ##unpackfield(obd)
    
if __name__ == "__main__":
    main(sys.argv[1:])