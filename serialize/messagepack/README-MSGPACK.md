#       MSGPACK    

#### Author: A.Formica     
##### Date of last development period: 2016/10/09 
```
   Copyright (C) 2016  A.Formica

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

# Table of Contents
1. [Description](#description)
2. [Msgpack](#msgpack)
3. [Run](#run)

## Description
The examples in this directory are using some alignment CLOBs and Msgpack library to serialize and deserialize them.

## Msgpack
You should install msgpack for python and C++ in order to run these examples.
You can find better documentation for this in http://msgpack.org .
In this virtual machine (conddb06) we have installed the development tools for CentOS 7.
```
   git clone https://github.com/msgpack/msgpack-c.git
   cd msgpack-c
   cmake -DMSGPACK_CXX11=ON .
   sudo yum install centos-release-scl
   cmake -DMSGPACK_CXX11=ON .
   sudo yum install devtoolset-4
   cmake -DMSGPACK_CXX11=ON .
   sudo yum install cmake
   cmake -DMSGPACK_CXX11=ON .
   sudo make install
```

## Run
To run the examples in cpp you should create the libraries to parse the alignment CLOB. 
Go to cpp/build and run cmake:
```
cd cpp/build
cmake ..
make 
```
The executable are called 
```
testcool
```
and 
```
testmsgpk-aline
```
To measure the time reading the CLOB or the JSON equivalent in msgpack you should use the command time.
In order to be more realistic there is a script executing N times the command, which will for sure give
more stable and realistic results. 
```
time ./test_loop.sh ./cpp/build/testcool 100
```
The command above can be used to call 100 times the testcool executable. This is reading the file test.txt in the local directory.
The file should be linked from 
```
../data/test.txt
```
The same test can be performed with message pack JSON by calling the executable:
```
time ./test_loop.sh ./cpp/build/testmsgpk-aline 100
```
this time you should have linked the file 
```
../data/test.bin
```

