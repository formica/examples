#include <msgpack.hpp>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include "MuonAlignmentData/ALinePar.h"
#include "MuonAlignmentData/BLinePar.h"

class ALinePar;
class BLinePar;

class alignblob {
private:
    //std::map<std::string,ALinePar> m_a;
    //std::map<std::string,BLinePar> m_b;
    ALinePar A;
    BLinePar B;
    std::vector<int> AMDBID;
    std::string TYPE;
    std::string CHAMBER;
    
public:
    void setData(ALinePar al, BLinePar bl, std::vector<int> amdb, std::string type, std::string chamber) {
        //m_a.insert(std::make_pair(a,al));
        //m_b.insert(std::make_pair(b,bl));
        A = al;
        B = bl;
        TYPE = type;
        AMDBID = amdb;
        CHAMBER = chamber;
    }
    MSGPACK_DEFINE_MAP(A,B,AMDBID,CHAMBER,TYPE);
};

class alignheader {
private:
    std::string header;
    std::string info;
    
public:
    void setData(std::string h, std::string i) {
        header = h;
        info = i;
    }
    MSGPACK_DEFINE_MAP(header,info);
};


class aligndata {
private:
    std::string iovid;
    std::vector<alignblob> data;
    std::vector<alignheader> header;
    
public:
    void setData(std::string id, std::vector<alignblob> md, std::vector<alignheader> mh) {
       iovid = id;
       data = md;
       header = mh;
    }
    void getData(std::string &id, std::vector<alignblob> &md, std::vector<alignheader> &mh) {
       id = iovid;
       md = data;
       mh = header;
    }
    MSGPACK_DEFINE_MAP(iovid,header,data);
};


int main(void) {
        //std::vector<ALinePar> vecal;
 /*       std::vector<alignblob> vecal;
    
        // add some elements into vec...
        std::vector<int> amdb;
        amdb.push_back(4);
        amdb.push_back(6);
        amdb.push_back(0);

        ALinePar a1;
        a1.setParameters(-0.969, 3.168, -0.007, 0.001532, 0.000397, 1.1e-05);
        //vecal.push_back(a1);
        BLinePar b1;
        b1.setParameters(-0.089, 0.122, 0.03, 0.0, 0.0, 0.397, 1.27, 0.0, 0.092, 0.011, -0.005);
        alignblob blob;
        blob.setData(a1,b1,amdb,"EOS","EOS6A08");
        vecal.push_back(blob);
        //vecal.push_back(a2);
    
        // you can serialize myclass directly
        msgpack::sbuffer sbuf;
        msgpack::pack(sbuf, vecal);

        msgpack::object_handle oh =
            msgpack::unpack(sbuf.data(), sbuf.size());

        msgpack::object obj = oh.get();
        std::cout << "Found object " << obj << std::endl;
        // you can convert object to myclass directly
        std::vector<alignblob> rvec;
        obj.convert(rvec);
*/    
        std::cout << "Now read existing file..." << std::endl;
  std::ifstream is ("./test.bin", std::ifstream::binary);
  if (is) {
    // get length of file:
    is.seekg (0, is.end);
    int length = is.tellg();
    is.seekg (0, is.beg);

    char * buffer = new char [length];
    msgpack::sbuffer sbuf;

    std::cout << "Reading " << length << " characters... ";
    // read data as a block:
    is.read (buffer,length);

    if (is)
      std::cout << "all characters read successfully." << std::endl ;
    else
      std::cout << "error: only " << is.gcount() << " could be read";
    is.close();

    // ...buffer contains the entire file...
      
    // deserialize it.
    msgpack::object_handle ohd =
    msgpack::unpack(buffer, length);
 // print the deserialized object.
    msgpack::object objd = ohd.get();
    //std::cout << objd << std::endl;  //=> ["Hello", "MessagePack"]

    //std::cout << "Found object " << objd << std::endl;
    // you can convert object to myclass directly
    aligndata rdata;
    objd.convert(rdata);     
    std::string iovid;
    std::vector<alignblob> ab;
    std::vector<alignheader> ah;
    rdata.getData(iovid,ab,ah);
      
    std::cout << "Retrieved data and header of size: " << ab.size() << " and " << ah.size() << " for iovid " << iovid << std::endl;
  }      
}

