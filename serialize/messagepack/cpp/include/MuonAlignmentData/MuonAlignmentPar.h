#ifndef MUONALIGNMENTDATA_MUONALIGNMENTPAR_H
#define MUONALIGNMENTDATA_MUONALIGNMENTPAR_H


#include <string>


class MuonAlignmentPar {

 public:

  MuonAlignmentPar();
  ~MuonAlignmentPar();
  
  // Full constructor
  //  MuonAlignmentPar(Identifier stationId, SimpleTime sinceTime, SimpleTime tillTime);
  
  void setAmdbId(std::string type, int jff, int jzz, int job);
  void getAmdbId(std::string& type, int& jff, int& jzz, int& job);

  bool isNew()              {return m_isNew;}
  void isNew(bool newPar)   {m_isNew = newPar;}
  

  
 protected:
  
  // Amdb identifier 
  std::string m_Type;
  int m_Jff;
  int m_Jzz;
  int m_Job;
  


  bool m_isNew;

};



#endif  // MUONALIGNMENTDATA_MUONALIGNMENTPAR_H
