#ifndef MUONALIGNMENTDATA_ALINEPAR_H
#define MUONALIGNMENTDATA_ALINEPAR_H

#include <msgpack.hpp>

#include "MuonAlignmentData/MuonAlignmentPar.h"

class ALinePar : public MuonAlignmentPar {

 private:

  // traslation parameters
  float m_S;
  float m_Z;
  float m_T;
  // rotation parameters
  float m_rotS;
  float m_rotZ;
  float m_rotT;
    
 public:

  // Default constructor
  ALinePar();
  // destructor
  ~ALinePar();

  // Full constructor


  void setParameters(float s, float z, float t, 
		     float rotS, float rotZ, float rotT);

  void getParameters(float& s, float& z, float& t, 
		     float& rotS, float& rotZ, float& rotT);

  MSGPACK_DEFINE(m_S, m_Z, m_T, m_rotS, m_rotZ, m_rotT);

};

#endif  // MUONALIGNMENTDATA_ALINEPAR_H
