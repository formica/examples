#ifndef MUONCONDSVC_MDTSTRINGUTILS_H
#define MUONCONDSVC_MDTSTRINGUTILS_H

class MdtStringUtils {
 public:
  static void tokenize(const std::string& str,
		       std::vector<std::string>& tokens,
        	       const std::string& delimiters = " ");
}; 

#endif
