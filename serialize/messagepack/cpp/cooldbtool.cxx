#include <fstream>
#include <iostream>
#include <vector>

#include "MdtStringUtils.h"
#include "MuonAlignmentData/ALinePar.h"
#include "MuonAlignmentData/BLinePar.h"

void loadABlines(std::string);

int main() {

  std::ifstream is ("test.txt", std::ifstream::binary);
  if (is) {
    // get length of file:
    is.seekg (0, is.end);
    int length = is.tellg();
    is.seekg (0, is.beg);

    char * buffer = new char [length];

    std::cout << "Reading " << length << " characters... ";
    // read data as a block:
    is.read (buffer,length);

    if (is)
      std::cout << "all characters read successfully." << std::endl ;
    else
      std::cout << "error: only " << is.gcount() << " could be read";
    is.close();
    // ...buffer contains the entire file...
    std::string blob(buffer);
    loadABlines(blob);
      
    delete[] buffer;
  }    

}


void loadABlines(std::string data) {

    // unpack the strings in the collection and update the 
    // ALlineContainer in TDS
    int nLines = 0;
    int nDecodedLines = 0;
    int nNewDecodedALines = 0;
    int nNewDecodedBLines = 0;
    int m_verbose = 0;
    //std::cout << "Data load is " << data << " FINISHED HERE "<<std::endl;
    bool hasBLine = true;

    // Check the first word to see if it is a correction
    std::string type;

    //Parse corrections
    std::string since_str;
    std::string till_str;
    std::string delimiter = "\n";

    std::vector<ALinePar> al_lines;
    std::vector<BLinePar> bl_lines;


    std::vector<std::string> lines;
    MdtStringUtils::tokenize(data,lines,delimiter);
    std::cout << "Found n lines " << lines.size() << std::endl;
    for (unsigned int i=0; i<lines.size();i++) {
        ++nLines;
	//std::cout<<"scanning CLOB --- current line = "<<nLines<<std::endl;
	//	ALinePar* newALine = new ALinePar();
        std::string blobline = lines[i];
        std::string delimiter = ":";
        
        std::vector<std::string> tokens;
        MdtStringUtils::tokenize(blobline,tokens,delimiter);
        type = tokens[0];
        //Parse line
        if (type.find("#")==0) {
            //skip it
            continue;
        }
        //std::cout << type ;
        if (type.find("Header")==0) {
            std::string delimiter = "|";
            std::vector<std::string> tokens;
            MdtStringUtils::tokenize(blobline,tokens,delimiter);
            since_str = tokens[1];
            till_str = tokens[2];
	  //std::cout << "Since: " << since_str << std::endl;
	  //std::cout << "Till: " << till_str << std::endl;
        }
	
        if (type.find("IOV")==0) {
            //std::cout<<" trovato IOV"<<std::endl;
            std::string delimiter = " ";
            std::vector<std::string> tokens;
            MdtStringUtils::tokenize(blobline,tokens,delimiter);
            if (m_verbose){
                for (unsigned int i=0; i<tokens.size(); i++) {	
                    std::cout << tokens[i] <<" | ";
                }
                std::cout << std::endl;
            }   
            int ival = 1;
            //long int iovThisBlob=0;
            //long int lastIOV = getLastIOVforThisFolder(folderName);

            //std::string str_iovThisBlob = tokens[ival];
            //sscanf(str_iovThisBlob.c_str(),"%80ld",&iovThisBlob);
        }
	
        if (type.find("Corr")==0) {
//#: Corr line is counter typ,  jff,  jzz, job,                         * Chamber information 
//#:                       svalue,  zvalue, tvalue,  tsv,  tzv,  ttv,   * A lines 
//#:                       bz, bp, bn, sp, sn, tw, pg, tr, eg, ep, en   * B lines 
//#:                       chamber                                      * Chamber name 
//.... example
//Corr: EMS  4   1  0     2.260     3.461    28.639 -0.002402 -0.002013  0.000482    -0.006    -0.013 -0.006000  0.000000  0.000000     0.026    -0.353  0.000000  0.070000  0.012000    -0.012    EMS1A08

	    //std::cout<<" trovato Corr"<<std::endl;
            std::string delimiter = " ";
            std::vector<std::string> tokens;
            MdtStringUtils::tokenize(blobline,tokens,delimiter);
            if( m_verbose ) {
                std::cout << "Parsing Line = ";
                for (unsigned int i=0; i<tokens.size(); i++) std::cout <<tokens[i] <<" | ";
                std::cout<<std::endl;
            }	  
            bool thisRowHasBLine = true;
            if (tokens.size()<15) {
                std::cout <<"(old COOL blob convention for barrel) skipping B-line decoding "<<std::endl;
                return;
            }

            // Start parsing
            int ival=1;

	  // Station Component identification 
	  int jff; 
	  int jzz;
	  int job;
	  std::string stationType = tokens[ival++];	  
	  std::string jff_str = tokens[ival++]; 
	  sscanf(jff_str.c_str(),"%80d",&jff);
	  std::string jzz_str = tokens[ival++];
	  sscanf(jzz_str.c_str(),"%80d",&jzz);
	  std::string job_str = tokens[ival++];
	  sscanf(job_str.c_str(),"%80d",&job);
	  
	  // A-line
	  float s;
	  float z;
	  float t;
	  float ths;
	  float thz;
	  float tht;	  
	  std::string s_str = tokens[ival++];
	  sscanf(s_str.c_str(),"%80f",&s);
	  std::string z_str = tokens[ival++];
	  sscanf(z_str.c_str(),"%80f",&z);
	  std::string t_str = tokens[ival++];
	  sscanf(t_str.c_str(),"%80f",&t);
	  std::string ths_str = tokens[ival++];
	  sscanf(ths_str.c_str(),"%80f",&ths);
	  std::string thz_str = tokens[ival++];
	  sscanf(thz_str.c_str(),"%80f",&thz);
	  std::string tht_str = tokens[ival++];
	  sscanf(tht_str.c_str(),"%80f",&tht);

	  // B-line
	  float bz, bp, bn, sp, sn, tw, pg, tr, eg, ep, en;
	  std::string ChamberHwName="";
	  
	  if (hasBLine && thisRowHasBLine)
	  {	      
	  std::string tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&bz);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&bp);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&bn);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&sp);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&sn);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&tw);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&pg);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&tr);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&eg);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&ep);
	  tmp_str = tokens[ival++];
	  sscanf(tmp_str.c_str(),"%80f",&en);	  

	  // ChamberName (hardware convention)
	  ChamberHwName = tokens[ival++];
	  }
            ALinePar* newALine = new ALinePar();
            newALine->setAmdbId(stationType, jff, jzz, job);
            newALine->setParameters(s,z,t,ths,thz,tht);
            newALine->isNew(true);	  
            al_lines.push_back(*newALine);
            
          if (hasBLine && thisRowHasBLine)
	  {
	      // new Bline
	      BLinePar* newBLine = new BLinePar();
	      newBLine->setAmdbId(stationType, jff, jzz, job);
	      newBLine->setParameters(bz, bp, bn, sp, sn, tw, pg, tr, eg, ep, en);
	      newBLine->isNew(true);
          bl_lines.push_back(*newBLine);      
	  }
	}
    }
    std::cout << "Number of alines " << al_lines.size() << " blines " << bl_lines.size() << std::endl;
}
  