#include "MuonAlignmentData/ALinePar.h"

ALinePar::ALinePar():
  MuonAlignmentPar(),
  m_S(0.0),
  m_Z(0.0),
  m_T(0.0),
  m_rotS(0.0),
  m_rotZ(0.0),
  m_rotT(0.0)
{ }

ALinePar::~ALinePar()
{ }


void ALinePar::setParameters(float s, float z, float t, 
			     float rotS, float rotZ, float rotT)
{
  m_S = s;
  m_Z = z;
  m_T = t;
  m_rotS = rotS;
  m_rotZ = rotZ;
  m_rotT = rotT;
}

void ALinePar::getParameters(float& s, float& z, float& t, 
			     float& rotS, float& rotZ, float& rotT)
{
  s    = m_S;
  z    = m_Z;
  t    = m_T;
  rotS = m_rotS;
  rotZ = m_rotZ;
  rotT = m_rotT;
}

