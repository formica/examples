#       EXAMPLES

#### Author: A.Formica     
##### Date of last development period: 2016/10/09 
```
   Copyright (C) 2016  A.Formica

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

# Table of Contents
1. [Description](#description)
2. [Serialize](#serialize)

## Description
This project contains software examples using several libraries.

## Serialize
Some examples for (de)serialization libraries. 

1. messagepack => Test message pack json serialization library.
              Go to the [README file](serialize/messagepack/README-MSGPACK.md) for more informations.
